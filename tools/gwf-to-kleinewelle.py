#!/usr/bin/env python
usage       = "gwf-to-klienewelle.py [--options] path/to/gwf"
description = "converts a gwf file into a KlieneWelle config. Prints the resulting config file to stdout if --output-file is not specified. NOTE: relies on FrChannels via subprocess to extract channel lists."
author      = "reed.essick@ligo.org"

#-------------------------------------------------

import numpy as np

import re

import os
import sys

import subprocess as sp

import getpass

import time

from collections import defaultdict

from ConfigParser import SafeConfigParser

from optparse import OptionParser

#-------------------------------------------------

### define some useful variables

default_frqmap = {
        16384 : [(8,128), (32,2048), (1024,4096), (2048,8192)],
         8192 : [(8,128), (32,2048), (1024,4096), (2048,4096)],
         4096 : [(8,128), (32,2048), (1024,2048)],
         2048 : [(8,128), (32,1024)],
         1024 : [(8,128), (32,512)],
          512 : [(8,128), (32,256)],
          256 : [(8,128)],
        }

kwconfigHeader = """\
stride         %(stride)d
basename       %(basename)s_TRIGGERS
segname        %(basename)s_SEGMENTS
significance   %(signif).1f
threshold      %(threshold).1f
decimateFactor %(decimate).0f"""

#------------------------

### define some useful methods

def gwf2chans( gwf ):
    '''
    extract and format channels and sampling frequencies from a gwf file
    '''
    proc = sp.Popen( ['FrChannels', gwf], stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = proc.communicate()

    if proc.returncode:
        raise RuntimeError( 'FrChannels returncode=%d\n%s\n%s'%(proc.returncode, out, err) )

    ### format into a dictionary with (channelName : samplingFreq) pairs
    chans = dict( line.strip().split() for line in out.strip().split('\n') )
    for key, val in chans.items():
        chans[key] = int(val)

    return chans

#-------------------------------------------------

parser = OptionParser(usage=usage, description=description)

### verbosity options
parser.add_option('-v', '--verbose', default=False, action='store_true',
    help='print information' )

parser.add_option('-V', '--Verbose', default=False, action='store_true',
    help='print even more information' )

parser.add_option('-o', '--output-file', default=None, type='string',
    help='path to new OmegaScan config. If not specified, config will be printed to stdout' )

### options about specific channels
parser.add_option('', '--channel-exclude', default=[], action='append', type='string',
    help='exclude this channel (requires exact match). Can be repeated' )

parser.add_option('', '--regex-channel-exclude', default=[], action='append', type='string',
    help='exclude all channels that match this regular expression. Can be repeated' )

### options to override things in default_frqmap
parser.add_option('', '--freq-map', default=[], nargs=3, action='append', type='int',
    help='overwrite the default frequency mapping from sampling frequency to analysis bandwidths \
(eg: "--freq-map 1024 32 512" will add an analysis band between 32-512 Hz for channels with sampling \
frequencies of 1024 Hz). This argument can be repeated, but if supplied even once will overwrite \
the entire default frequency map. NOTE: if a channel\'s sampling frequency is not present in the \
frequency map, it is not included in the resulting config.' )

parser.add_option('', '--f-low', default=None, type='float',
    help='the low frequency cuttoff used in search. DEFAULT=None and this value will be based only on the default frequency map.\
If supplied, this will be applied to all channels independent of the sampling frequencies. Will also overwrite anything specified \
with --freq-map' )

parser.add_option('', '--f-high', default=None, type='float',
    help='the maximum allowed frequency used in the search. DEFAULT=None and this value will be based only on the default frequency map.\
If supplied, this will be applied to all channels independent of the sampling frequencies. Will also overwrite anything specified \
with --freq-map' )

### options specific to KlieneWelle configs
parser.add_option('', '--basename', default=None, type='string',
    help='the basename for the resulting KW triggers. Default=None and will be based of gwf name' )

parser.add_option('', '--stride', default=None, type='int', 
    help='the stride for the KW analysis. Default=None and will be based on the lowest frequency analyzed.\
If supplied, should be a power of 2' )

parser.add_option('', '--signif', default=15.0, type='float',
    help='the significance threshold for trigger production. Default=15.0' )

parser.add_option('', '--threshold', default=3.0, type='float',
    help='the \"threshold\" parameter in the config\'s header. Default=3.0' )

parser.add_option('', '--decimate', default=-1, type='int',
    help='the \"decimateFactor" parameter in the config\'s header. Default=-1' )

opts, args = parser.parse_args()

#------------------------

### parse out chanlist config name
if len(args)!=1:
    raise ValueError('please supply exactly 1 input argument\n%s'%usage)
gwf = args[0]

### verbosity
opts.verbose = opts.verbose or opts.Verbose

### set up regex channel exclusion
opts.regex_channel_exclude = [re.compile(_) for _ in opts.regex_channel_exclude]

#-------------------------------------------------

if opts.verbose:
    print( 'reading channels from : %s'%gwf )

channels = gwf2chans( gwf )

if opts.Verbose:
    for chan, fs in channels.items():
        print "    %8.1f\t%s"%(fs, chan)

### remove channels we don't want
for chan in opts.channel_exclude:
    #     it was in channels          we're printing a lot of stuff
    if channels.pop(chan, None) and opts.Verbose:
        print( 'excluding : %s'%chan )

for chan in channels.keys():
    for regex in opts.regex_channel_exclude:
        if regex.match(chan):
            channels.pop(chan)
            if opts.Verbose:
                print( 'excluding : %s'%chan )    
        
#-------------------------------------------------

### set up frequency map
if opts.freq_map:
    frqmap = defaultdict( list )
    for fs, fm, fM in opts.freq_map:
        frqmap[fs].append( [fm, fM] )

else:
    frqmap = default_frqmap

if opts.f_low!=None: ### supplied, so we need to update the frequency mapping
    if opts.verbose:
        print( 'updating frequency map to only include analysis frequencies above %.3f Hz'%opts.f_low )
    frqmap = dict( (key, [ [max(opts.f_low, fm), fM] for fm, fM in val]) for key, val in frqmap.items() )

if opts.f_high!=None: ### supplied, so we need to update the frequency mapping
    if opts.verbose:
        print( 'updating frequency map to only include analysis frequencies below %.3f Hz'%opts.f_high )
    frqmap = dict( (key, [ [fm, min(opts.f_high, fM)] for fm, fM in val]) for key, val in frqmap.items() )

### clean up frequency map to make sure it's valid
if opts.Verbose:
    print( 'confirming that frequency map is sane' )
for key in frqmap.keys():
    val = [ [fm, fM] for fm, fM in frqmap.pop(key) if fm < fM ] ### downselect based on basic sanity requirement
    if val: ### if there's still something there, add it back in
        frqmap[key] = val 

if not frqmap: ### no keys left!
    raise ValueError( 'no valid frequency maps retained!' )

if opts.Verbose:
    print( 'using frequency map:' )
    for key, val in frqmap.items():
        print( 'sampling frequency : %.1f'%key )
        for fm, fM in val:
            print( '    fmin : %10.1f\tfmax : %10.1f'%(fm, fM) )

#-------------------------------------------------

### set up stride
if opts.stride==None: ### not supplied, need to set based on lowest analysis frequency
    fm = min( [ fm for val in frqmap.values() for fm, fM in val ] )
    ### require 64 "independent trials" for statistics/whitening
    ### ensure stride is actually a power of 2, cast to an integer
    opts.stride = int( 2**np.ceil( np.log(64./fm)/np.log(2) ) )
    
#-------------------------------------------------

### set up basename
if opts.basename==None:
    opts.basename = os.path.basename( gwf ).strip('.gwf').split('-')[1].replace('_', '-') ### assumes standard ligo naming convention

#-------------------------------------------------
### iterate over channels, adding them to the config string
#-------------------------------------------------

config = kwconfigHeader%{
    'stride'    : opts.stride,
    'basename'  : opts.basename,
    'signif'    : opts.signif,
    'threshold' : opts.threshold,
    'decimate'  : opts.decimate,
}

for chan in sorted(channels.keys()): ### iterate over channels and add each to the config
    fs = channels[chan]
    if frqmap.has_key(fs):
        for fm, fM in frqmap[fs]:
            config += "\nchannel %-30s %8d %8d"%(chan, fm, fM)

#-------------------------------------------------
### write out the config file
if opts.output_file:
    if opts.verbose:
        print( "writing KW config to : %s"%opts.output_file )
    file_obj = open(opts.output_file, 'w')
    file_obj.write( config ) ### NOTE: this way sorta needlessly uses more memory than necessary because I don't every need the full string at once...
    file_obj.close()

else:
    print >> sys.stdout, config
